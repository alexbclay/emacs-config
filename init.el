;; Alex's .emacs file

;; ===== Set up basics =====


(setq inhibit-startup-screen t)
(tool-bar-mode -1)
;; create closing item when typing the starting one (parens, quotes, etc)
(electric-pair-mode 1)
;; no scrollbars
(scroll-bar-mode -1)
;; no tabs!
(setq-default indent-tabs-mode nil)

;; any tabs should be 4 spaces
(setq-default tab-width 4)

;; show paren matching
(show-paren-mode 1)
(setq show-paren-delay 0)
(setq show-paren-style 'mixed)

;; for debugging issues with packages
;; (setq debug-on-error t)

;; ===== Packages! =====
;; see use-package docs here: https://github.com/jwiegley/use-package

(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)

;; Install use-package if not already installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

;; always install missing packages
(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; ===== Solarized theme =====

(use-package solarized-theme)
;; customizations based on https://github.com/bbatsov/solarized-emacs
;; make the modeline high contrast
(setq solarized-high-contrast-mode-line t)

(load-theme 'solarized-dark t)

;; ===== General packages for stuff ====

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package treemacs)

;; ===== Add Tree-sitter =====
;; look into this more at https://www.masteringemacs.org/article/how-to-get-started-tree-sitter

(use-package tree-sitter)
(use-package tree-sitter-langs)
(global-tree-sitter-mode)
(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)

(setq treesit-language-source-alist
      '((bash "https://github.com/tree-sitter/tree-sitter-bash")
        (elisp "https://github.com/Wilfred/tree-sitter-elisp")
        (python "https://github.com/tree-sitter/tree-sitter-python")
        ;; Just use the rustic mode for now, doesn't integrate well with tree sitter
        ;; (rust "https://github.com/tree-sitter/tree-sitter-rust")
        (yaml "https://github.com/ikatyang/tree-sitter-yaml")
        (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.3" "typescript/src")
        )
      )
(setq major-mode-remap-alist
 '((yaml-mode . yaml-ts-mode)
   (bash-mode . bash-ts-mode)
   (typescript-mode . typescript-ts-mode)
   (python-mode . python-ts-mode),
   (rust-mode . rust-ts-mode)))

;; ===== Deal with whitespace =====

(use-package whitespace
  :config
  (setq whitespace-line-column 120)
  (setq whitespace-style '(face lines-tail))
  (global-whitespace-mode 1))
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq show-trailing-whitespace t)


;; ===== Make buffer names unique ====
;; Probably not needed anymore??

;;(use-package uniquify
;; :config
;; (setq uniquify-buffer-name-style 'post-forward))

;; ===== ido mode =====

;; always use ido (interactive do things) mode
(use-package ido
  :config
  (ido-mode t)
  (setq ido-enable-flex-matching t)
  (setq ido-everywhere t))

;; ===== git stuff =====

(use-package magit)
(global-set-key (kbd "C-x g") 'magit-status)

;; ===== LSP stuff =====
(use-package company)
(use-package lsp-mode
  :ensure
  :commands lsp
  :custom
  ;; what to use when checking on-save. "check" is default, I prefer clippy
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.6)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode)
  (setq lsp-keep-workspace-alive nil))

(use-package lsp-ui
  :ensure
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable nil))

;; ===== python stuff =====
;; (use-package poetry
;;  :ensure t)

;; ;; make flycheck use pylint
;; (add-hook 'python-mode-hook #'(lambda () (setq flycheck-checker 'python-pylint)))

;; (use-package elpy
;;   :init
;;   (elpy-enable))

;; ;; use the current virtualenv as the python rpc
;; (setq elpy-rpc-virtualenv-path 'current)

;; (use-package lsp-jedi
;;   :ensure t
;;   :hook (python-mode . (lambda ()
;;                           (require 'lsp-jedi)
;;                           (lsp)))   ; or lsp-deferred
;;   )
(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp)))   ; or lsp-deferred
  :custom
  (lsp-pyright-venv-path ".")
  )

(use-package python-black
  :demand t
  :after python
  :hook (python-mode . python-black-on-save-mode-enable-dwim))

;; Don't try to guess the indentation offset
(setq python-indent-guess-indent-offset nil)

;; switch out flymake for flycheck
;; see https://elpy.readthedocs.io/en/latest/customization_tips.html?#use-flycheck-instead-of-flymake
;; (when (load "flycheck" t t)
;;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
;;   (add-hook 'elpy-mode-hook 'flycheck-mode))

;; ;; Auto-format on save
;; (add-hook 'elpy-mode-hook (lambda ()
;;                             (add-hook 'before-save-hook
;;                                       'elpy-format-code nil t)))
;; ===== JS Stuff =====

(use-package json-mode)
(use-package prettier)
(add-hook 'js-mode-hook 'prettier-mode)
(add-hook 'json-mode-hook 'prettier-mode)
(add-hook 'markdown-mode-hook 'prettier-mode)
(add-hook 'yaml-mode-hook 'prettier-mode)
(add-hook 'typescript-mode-hook 'prettier-mode)

;; Stolen from https://github.com/ramblehead/.emacs.d/blob/master/init.el#L3260
;; (use-package yarn-pnp
;;   :load-path "lisp/yarn-pnp.el"
;;   :pin manual
;;   :defer t)

;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)

(add-hook 'typescript-mode-hook 'lsp)

;; global prettier makes python use 2 spaces for indentation...
;; (add-hook 'after-init-hook #'global-prettier-mode)
(setenv "NODE_PATH" "/Users/alexbclay/.config/yarn/global/node_modules/")

;; ===== Rust stuff =====
;; https://github.com/emacs-rustic/rustic?tab=readme-ov-file#installation
(use-package quelpa-use-package
  :ensure t)

(use-package rustic
  :quelpa (rustic :fetcher github
                  :repo "emacs-rustic/rustic")
  :bind (:map rustic-mode-map
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ))


;; Most of this is from https://github.com/rksm/emacs-rust-config
;; also https://robert.kra.hn/posts/2021-02-07_rust-with-emacs/
;; (use-package rustic
;;   :ensure
;;   :bind (:map rustic-mode-map
;;               ("M-j" . lsp-ui-imenu)
;;               ("M-?" . lsp-find-references)
;;               ("C-c C-c l" . flycheck-list-errors)
;;               ("C-c C-c a" . lsp-execute-code-action)
;;               ("C-c C-c r" . lsp-rename)
;;               ("C-c C-c q" . lsp-workspace-restart)
;;               ("C-c C-c Q" . lsp-workspace-shutdown)
;;               ("C-c C-c s" . lsp-rust-analyzer-status))
;;   :config
;;   ;; uncomment for less flashiness
;;   ;; (setq lsp-eldoc-hook nil)
;;   ;; (setq lsp-enable-symbol-highlighting nil)
;;   ;; (setq lsp-signature-auto-activate nil)

;;   ;; comment to disable rustfmt on save
;;   ;; (setq rustic-format-on-save t)
;;   ;; (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))
;; )
;; (defun rk/rustic-mode-hook ()
;;   ;; so that run C-c C-c C-r works without having to confirm, but don't try to
;;   ;; save rust buffers that are not file visiting. Once
;;   ;; https://github.com/brotzeit/rustic/issues/253 has been resolved this should
;;   ;; no longer be necessary.
;;   (when buffer-file-name
;;     (setq-local buffer-save-without-query t)))

;; ===== Scala stuff =====

;; metals needs treemacs

;; cherry-picked from https://scalameta.org/metals/docs/editors/emacs/

;; Enable scala-mode for highlighting, indentation and motion commands
;; (use-package scala-mode
;;   :interpreter ("scala" . scala-mode))

;; Enable sbt mode for executing sbt commands
;; (use-package sbt-mode
;;   :commands sbt-start sbt-command
;;   :config
;;   ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
;;   ;; allows using SPACE when in the minibuffer
;;   (substitute-key-definition
;;    'minibuffer-complete-word
;;    'self-insert-command
;;    minibuffer-local-completion-map)
;;    ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
;;    (setq sbt:program-options '("-Dsbt.supershell=false")))

;; Add metals backend for lsp-mode
;; (use-package lsp-metals
;;   :hook (scala-mode . lsp)
;; )

;; ===== Other stuff =====

(use-package csv-mode)
(use-package yaml-mode)
(use-package toml-mode)

;; ===== Snippets =====

(use-package yasnippet)
(use-package yasnippet-snippets)
(yas-global-mode 1)
(setq yas-prompt-functions '(yas-ido-prompt))

;; ===== Highlights =====

(global-hi-lock-mode 1)
(defun highlight-todos ()
  (interactive)
  (highlight-regexp "\\S?[Tt][Oo][Dd][Oo].*" 'hi-blue)
  (highlight-lines-matching-regexp "FIXME" 'hi-red-b)
  (highlight-regexp "ABC:.*" 'hi-green)
  (highlight-regexp "XXX" 'hi-red-b)
  (highlight-lines-matching-regexp "pp\(rint\)?(].*" 'hi-pink)
  (highlight-lines-matching-regexp "print[ (]" 'hi-pink)
  (highlight-lines-matching-regexp "pdb" 'hi-pink)
  (highlight-lines-matching-regexp ".*logger.*" 'hi-blue-b)
  (highlight-lines-matching-regexp "cProfile" 'hi-yellow)
  )

(add-hook 'find-file-hook 'highlight-todos)

;; ====== I like my comment-block command this way =====

(defun comment-eclipse ()
  (interactive)
  (let ((start (line-beginning-position))
        (end (line-end-position)))
    (when (or (not transient-mark-mode) (region-active-p))
      (setq start (save-excursion
                    (goto-char (region-beginning))
                    (beginning-of-line)
                    (point))
            end (save-excursion
                  (goto-char (region-end))
                  (end-of-line)
                  (point))))
    (comment-or-uncomment-region start end)))
(global-set-key (kbd "C-?") 'comment-eclipse)


;; ====== AUTOMATICALLY ADDED STUFF BELOW HERE =====

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c" "4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3" default))
 '(package-selected-packages
   '(tree-sitter-langs tree-sitter yasnippet-snippets yaml-mode toml-mode solarized-theme rustic quelpa-use-package python-black prettier poetry magit lsp-ui lsp-pyright lsp-metals json-mode flycheck csv-mode company))
 '(safe-local-variable-values
   '((eval let
           ((project-directory
             (car
              (dir-locals-find-file default-directory))))
           (setq lsp-clients-typescript-server-args
                 `("--tsserver-path" ,(concat project-directory ".yarn/sdks/typescript/bin/tsserver")
                   "--stdio")))
     (eval pipenv-activate))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
